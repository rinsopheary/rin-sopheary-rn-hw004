import React, { Component } from 'react'
// import { Text, StyleSheet, View } from 'react-native'
import { Card, Textarea } from 'native-base'
import { StyleSheet, Text, View, Image, Button, TextInput } from 'react-native'
import ImagePicker from 'react-native-image-picker';


const options = {
    title: 'Select your option',
    // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };
  

export default class AddStoryPage extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             imageSource : require('../assets/images/defalutimage.png')
        }
    }
    
    imageHandler = ()=>{
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);
          
            if (response.didCancel) {
              console.log('User cancelled image picker');
            } else if (response.error) {
              console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
              console.log('User tapped custom button: ', response.customButton);
            } else {
              const source = { uri: response.uri };
              
    
          
              // You can also display the image using data:
              // const source = { uri: 'data:image/jpeg;base64,' + response.data };
          
              this.setState({
                imageSource: source,
              });
            }
          });
      }

    render() {
        return (
                    <View style={styles.container}>
            <Card style={styles.imageContainer}>
                <Image
                    style={styles.imageStyle}
                    source={this.state.imageSource}
                />
            </Card>

            <Button
                title={'Select File'}
                onPress = {()=>this.imageHandler()}
            />

            <Textarea
                bordered
                style={{ height: 230 }}
                placeholder="Write your text post here....."
                placeholderTextColor={'black'}
            />
            {/* <TextInput 
                style={textInputStyle}
                placeholder = "Write your text post here....."
                autoFocus
            /> */}

            <View>
            <Button
                title={'Draft'}
            />

            <Button
                title={'Public'}
            />
            </View>
        </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        padding: 5
    },
    imageContainer: {
        width: 400,
        height: 200
    },
    imageStyle: {
        width: '100%',
        height: '100%',
        resizeMode: "contain",
    },
    textInputStyle: {
        borderWidth: 1,
        borderColor: 'black',
        // height : 200
    }
})
