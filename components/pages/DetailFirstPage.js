import { Body, Container, Footer, FooterTab, Header } from 'native-base';
import React from 'react'
import { StyleSheet, Text, Image, Linking } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';

const DetailFirstPage = ({ route }) => {

    const { getIndex } = route.params
    const { container, titleStyle, authorStyle, createdStyle, imageStyle, detailStyle } = styles
    
    return (
        <Container style={container}>
            <Body>
                <Text style={titleStyle}>{getIndex.title}</Text>
                <Text style={authorStyle}>{getIndex.author}</Text>
                <Text style={createdStyle}>{getIndex.created}</Text>
                <Image style={imageStyle} source={getIndex.image} />
                <Text style={detailStyle}>{getIndex.detail}</Text>
            </Body>
            <Footer style={styles.footerStyle}>
                <Icon
                    name="twitter"
                    style={styles.iconStyle}
                    onPress={() => Linking.openURL('https://about.fb.com/company-info/')}
                />

                <Icon
                    name="telegram"
                    style={styles.iconStyle}
                    onPress={() => Linking.openURL('https://telegram.org/')}

                />
            </Footer>
        </Container>
    )
}

export default DetailFirstPage

const styles = StyleSheet.create({
    container: {
        padding: 20
    },
    titleStyle: {
        fontSize: 30,
        fontWeight: "bold",
        color: 'blue'
    },
    authorStyle: {
        fontSize: 20
    },
    createdStyle: {
        color: 'grey'
    },
    imageStyle: {
        width: 400,
        height: 300,
        resizeMode: "contain"
    },
    detailStyle: {
        fontSize: 15
    },
    footerStyle: {
        alignContent: "center",
        fontSize: 30
    },
    iconStyle: {
        fontSize: 30,
        padding: 10,
        color: 'blue'
    }

})
