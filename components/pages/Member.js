import { Button, Card } from 'native-base'
import React from 'react'
import { ScrollView, StyleSheet, Text, View } from 'react-native'

const Member = () => {
    const { container, headerContainerStyle, cardContainerStyle,
        headerStyle, textHeaderStyle, cardTitleStyle,
        cardTextStyle, buttonStyle,buttonStyle2, buttonTextStyle,buttonTextStyle2, termStyle, textTermStyle } = styles

    return (
        <ScrollView>
            <View 
        style={container}>
            <View style={headerContainerStyle}>
                <Text style={headerStyle}>Unlimited Reading</Text>
                <Text style={headerStyle}>Free for 1 Month</Text>
            </View>
            <Text style={textHeaderStyle}>Read on any device, ad-free, and offline. We'll remind you 3 days before your trail ends. Cancel anytime.</Text>
            <Card style={cardContainerStyle}>
                <Text style={cardTitleStyle}>$5 / Month</Text>
                <Text style={cardTextStyle}>First month free</Text>
                <Button full style={buttonStyle}>
                    <Text style={buttonTextStyle}>Start your free trail</Text>
                </Button>
            </Card>

            <View style={cardContainerStyle}>
                <Text style={cardTitleStyle}>$5/Month</Text>
                <Text style={cardTextStyle}>First month free</Text>
                <Button full style={buttonStyle2}>
                    <Text style={buttonTextStyle2}>Start your free trail</Text>
                </Button>
            </View>

            <Text style={termStyle}>Term & conditions</Text>

            <Text style={textTermStyle}>By using this site you will be deemed to have irrevocably agreed to these terms.

            You will also be subject to any further terms and conditions, which may be presented as you navigate this site. These too will be binding on you. By using this site, you also accept the binding nature of any variations which we may issue whether you are specifically advised of these or not. You can obtain the latest set of terms by contacting us.

            It is a condition of your use of this site that you provide such details as we request, and that these are accurate in all respects. We reserve the right to disallow your use of this site if the particulars provided are materially inaccurate, whether this is your fault or not.</Text>
        </View>
        </ScrollView>
    )
}

export default Member

const styles = StyleSheet.create({
    container : {
        padding : 15
    },
    headerContainerStyle : {
        justifyContent : "center",
        alignItems : "center",
        padding : 20
    },
    headerStyle: {
        fontSize : 30,
        marginBottom : 10
    },
    textHeaderStyle: {
        justifyContent : "center",
        alignItems : "center",
        textAlign : "center",
        fontSize : 18,
        lineHeight : 25
    },
    cardContainerStyle : {
        padding : 27,
        justifyContent : "center",
        alignItems : "center",
        textAlign : "center",
        marginTop : 30,
    },
    cardTitleStyle: {
        fontSize: 30,
        fontWeight : "bold",
    },
    cardTextStyle: {
        marginTop: 10,
        fontSize: 15,
        marginBottom : 10
    },
    buttonStyle : {
        // color : 'black'
        backgroundColor : 'black'
    },
    buttonStyle2: {
        backgroundColor : 'white',
        borderWidth : 2,
        borderColor : 'black'
    },
    buttonTextStyle2: {
        color : 'black',
        fontSize : 15
    },
    buttonTextStyle: {
        color : 'white',
        fontSize : 15
    },
    termStyle: {
        marginTop : 40,
        fontSize: 22,
        fontWeight : "bold",
        marginBottom : 20
    },
    textTermStyle: {
        fontSize : 18,
        lineHeight : 25
    }
})
