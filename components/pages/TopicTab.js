import { Card , Button} from 'native-base'
import React, { useState } from 'react'
import { Alert, StyleSheet, Text, View} from 'react-native'
import { FlatList } from 'react-native-gesture-handler'
import { set } from 'react-native-reanimated'

const TopicTab = () => {
    const [data, setData] = useState([
        {
            key : 1,
            story: 'Art',
            follow : 'Follow'
        },
        {
            key: 2,
            story: 'Film',
            follow : 'Follow'

        },
        {
            key : 3,
            story: 'Gaming',
            follow : 'Follow'

        },
        {
            key : 4, 
            story: 'Aventures',
            follow : 'Follow'

        },
        {
            key : 5, 
            story: 'Comic',
            follow : 'Follow'

        },
    ])

    const handleClick = (index) =>{
        const arr = [...data]
        if (index + 1 === arr[index].key) {
            arr[index].follow === 'Follow'
              ? (arr[index].follow = 'Following')
              : (arr[index].follow = 'Follow');
          }
          setData(arr);
          console.log(arr);
          
    }

    const {container, itemStyle} = styles
    return (
        <View style = {container}>
            <FlatList 
                data = {data}
                renderItem = {({item, index}) => (
                    <View style={itemStyle}>
                        <Text>{item.story}</Text>
                        <Button 
                        onPress = {()=>handleClick(index)}
                        style={{padding : 20}}>
                            <Text>{item.follow}</Text>
                        </Button>
                    </View>
                )}
            />
        </View>
    )
}

export default TopicTab

const styles = StyleSheet.create({
    container : {
        padding : 15
    },
    itemStyle : {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        marginBottom : 10
    }
})
